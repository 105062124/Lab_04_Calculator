var screen = document.getElementById('screen');
var reset = true;

function BtnHandle(e) {
    switch(e.value.toString()) {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '+':
        case '-':
        case '.':
        case '(':
        case ')':
            if(reset) {
                document.getElementById('screen').value = e.value.toString();
                reset = false;
            }
            else {
                document.getElementById('screen').value += e.value.toString();
            }
            break;
        case '0':
            if(document.getElementById('screen').value !== '0' && !reset) {
                document.getElementById('screen').value += e.value.toString();
                reset = false;
            }
            if(reset) {
                document.getElementById('screen').value = e.value.toString()
            }
            break;
        case '×':
        case '÷':
            if(reset) {
                document.getElementById('screen').value = e.value.toString() === '×' ? '*' : '/';
                reset = false;
            }
            else {
                document.getElementById('screen').value += e.value.toString() === '×' ? '*' : '/';
            }
            break;
        case '=':
            try {
                document.getElementById('screen').value = eval(document.getElementById('screen').value.toString());
            }
            catch(err) {
                document.getElementById('screen').value = 'error';
                reset = true;
            }
            break;
        case 'C':
            document.getElementById('screen').value = "0";
            reset = true;
            break;
    }
}